window.billPayListComponent = Vue.extend({
  template: `
  <style media="screen">
    .pago {
      color: green;
    }
    .nao-pago {
      color: red;
    }
  </style>
  <table border="1" cellspadding="10">
    <thead>
      <tr>
        <td>#</td>
        <th>Vencimento</th>
        <th>Nome</th>
        <th>Valor</th>
        <th>Paga</th>
        <th colspan="2">Ações</th>
      </tr>
    </thead>
    <tbody>
      <tr v-for="(index, o) in bills" track-by="$index">
        <td>{{ index + 1 }}</td>
        <td>{{ o.date_due | dateFormat 'pt-BR'}}</td>
        <td>{{ o.name | uppercase }}</td>
        <td>{{ o.value | numberFormat }}</td>
        <td class="minha-classe" :class="{'pago': o.done, 'nao-pago': !o.done}">
          {{ o.done | doneLabel }}
        </td>
        <td>
          <a v-link="{ name: 'bill-pay.update', params: { id: o.id }}">Editar</a>
        </td>
        <td>
          <a href="#" @click.prevent="deleteBill(o)">Excluir</a>
        </td>
      </tr>
    </tbody>
  </table>
  `,
  data() {
    return {
      bills: []
    }
  },
  created() {
    Bill.query().then((response) => {
      this.bills = response.data;
    });
  },
  methods: {
    deleteBill(bill) {
      if (confirm("Deseja excluir esta conta?")) {
        Bill.delete({id: bill.id}).then((response) => {
          this.bills.$remove(bill);
          this.$dispatch('change-info');
        });
      }
    }
  }
});
