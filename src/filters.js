Vue.filter('doneLabel', (value) => value == 0 ? "Não paga" : "Paga");

Vue.filter('statusGeneral', (value) => {
  if (value === false) {
    return 'Nenhuma conta a cadastrada';
  }
  if (!value) {
    return "Nenhuma conta a pagar";
  } else {
    return "Existem " + value + " contas a serem pagas";
  }
});

Vue.filter('uppercase', {
  read(value) {
    return value.toUpperCase();
  },
  write(value) {
    return value.toLowerCase();
  }
});

Vue.filter('numberFormat', {
  read(value, locale, currency) { // mostrar informação na view
    let number = 0;
    locale = locale ? locale : 'pt-BR';
    currency = currency ? currency : 'BRL';
    if (value && typeof value !== undefined) {
      let numberRegex = value.toString().match(/\d+(\.{1}\d{1,2}){0,1}/g);
      number = numberRegex ? numberRegex[0] : numberRegex;
    }
    return new Intl.NumberFormat(locale, {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2,
      style: 'currency',
      currency: currency
    }).format(number);
  },
  write(value, oldValue, locale) { // pegar o valor da view e converter para armazenar no model
    let number = 0;
    if (value.length > 0) {
      number = value.replace(/[^\d\,]/g, '')
        .replace(/\,/g,'.');
      number = isNaN(number) ? 0 : parseFloat(number);
    }
    return number;
  }
});

Vue.filter('dateFormat', {
    read(value, locale) {
      if (value && typeof value !== undefined) {
        if (!(value instanceof Date)) {
          let dateRegex  = value.match(/\d{4}\-\d{2}\-\d{2}/g);
          let dateString = dateRegex ? dateRegex[0] : dateRegex;
          if (dateString) {
            value = new Date(dateString+"T03:00:00");
          } else {
            return value;
          }
        }
        let options = {
          year: 'numeric',
          month: '2-digit',
          day: '2-digit',
        };
        return new Intl.DateTimeFormat(locale, options).format(value);
      }
      return value;
    },
    write(value, oldValue, locale) {
      let dateRegex = value.match(/\d{2}\/\d{2}\/\d{4}/g);
      if (dateRegex) {
        let dateString = dateRegex[0];
        let date = new Date(dateString.split('/').reverse().join('-') + 'T03:00:00');
        if (!isNaN(date.getTime())) {
          return date;
        }
      }
      return value;
    }
});
