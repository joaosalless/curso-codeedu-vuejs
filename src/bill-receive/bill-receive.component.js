window.billReceiveComponent = Vue.extend({
  components: {
    // 'menu-component': billReceiveMenuComponent,
  },
  template: `
  <h1>{{ title }}</h1>
  <!--<h3 :class="{'gray': status === false, 'green': status === 0, 'red': status > 0 }">
    {{status | statusGeneral}}
  </h3>
  <menu-component></menu-component>
  <router-view></router-view> -->
  `,
  data() {
    return {
      title: "Contas a receber"
    };
  },
  // computed: {
  //   status() {
  //     var bills = this.$root.$children[0].bills;
  //     if (!bills.length) {
  //       return false;
  //     }
  //     var count = 0;
  //     for (var i in bills) {
  //       if (!bills[i].done) {
  //         count++;
  //       }
  //     }
  //     return count;
  //   }
  // }
});
