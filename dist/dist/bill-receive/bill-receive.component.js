"use strict";

window.billReceiveComponent = Vue.extend({
  components: {
    // 'menu-component': billReceiveMenuComponent,
  },
  template: "\n  <h1>{{ title }}</h1>\n  <!--<h3 :class=\"{'gray': status === false, 'green': status === 0, 'red': status > 0 }\">\n    {{status | statusGeneral}}\n  </h3>\n  <menu-component></menu-component>\n  <router-view></router-view> -->\n  ",
  data: function data() {
    return {
      title: "Contas a receber"
    };
  }
});
//# sourceMappingURL=bill-receive.component.js.map