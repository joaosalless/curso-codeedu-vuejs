'use strict';

var router = new VueRouter();

router.map({
  '/bill-pays': {
    component: billPayComponent,
    subRoutes: {
      '/': {
        name: 'bill-pay.list',
        component: billPayListComponent
      },
      '/create': {
        name: 'bill-pay.create',
        component: billPayCreateComponent
      },
      '/:id/update': {
        name: 'bill-pay.update',
        component: billPayCreateComponent
      }
    }
  },
  '/bill-receives': {
    name: 'bill-receive',
    component: billReceiveComponent
  },
  '*': {
    component: billComponent
  }
});

router.start({
  components: {
    'bill-component': billComponent
  }
}, '#app');

router.redirect({
  '*': {
    component: billComponent
  }
});

// class Bill {
//   constructor(id, name) {
//     this._id = id;
//     this._name = name;
//   }
//
//   showVariables(texto = "nenhum nome") {
//     console.log(this.id);
//     console.log(this.name);
//     console.log(texto);
//   }
//
//   get id() {
//     return this._id;
//   }
//
//   get name() {
//     return this._name;
//   }
//
//   set id(id) {
//     return this._id = id;
//   }
//
//   set name(name) {
//     return this._name = name;
//   }
// }
//
// class BillPay extends Bill {
//   constructor (id, name, pago) {
//     super(id, name)
//     this._pago = pago;
//   }
//
//   get pago() {
//     return this._pago;
//   }
//
//   set pago(pago) {
//     return this._pago = pago;
//   }
//
//   showVariables(texto = "nenhum nome") {
//     super.showVariables();
//     console.log(this.pago);
//   }
// }
//
// let bill = new BillPay(1, "Supermercado", true);
// console.log(bill);
// bill.id = 1000;
// bill.name = "Fatura de cartão de crédito";
// // console.log(bill.id);
// // console.log(bill.name);
// // console.log(bill.pago);
// bill.showVariables();