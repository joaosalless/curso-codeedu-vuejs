'use strict';

window.billPayCreateComponent = Vue.extend({
  template: '\n  <form name="form" @submit.prevent="submit">\n    <label>Vencimento</label>\n    <input type="text" v-model="bill.date_due | dateFormat \'en-US\'"/>\n    <br/><br/>\n    <label>Nome</label>\n    <select v-model="bill.name">\n      <option v-for="o in names" value="{{ o }}">{{ o }}</option>\n    </select>\n    <br/><br/>\n    <label>Valor</label>\n    <input type="text" v-model="bill.value | numberFormat \'en-US\' \'USD\'"/>\n    <br/><br/>\n    <label>Paga</label>\n    <input type="checkbox" v-model="bill.done"/>\n    <br/><br/>\n    <input type="submit" value="Enviar">\n    <br/><br/>\n  </form>\n  ',
  data: function data() {
    return {
      formType: 'insert',
      names: ['Conta de luz', 'Conta de água', 'Conta de telefone', 'Supermercado', 'Cartão de crédito', 'Empréstimo', 'Gasolina'],
      bill: new BillPay()
    };
  },
  created: function created() {
    if (this.$route.name == 'bill-pay.update') {
      this.formType = 'update';
      this.getBill(this.$route.params.id);
      return;
    }
  },

  methods: {
    submit: function submit() {
      var _this = this;

      // var data = Vue.util.extend(this.bill, {date_due: this.getDueDate(this.bill.date_due)});
      var data = this.bill.toJSON();
      if (this.formType == 'insert') {
        Bill.save({}, data).then(function (response) {
          _this.$dispatch('change-info');
          _this.$router.go({ name: 'bill-pay.list' });
        });
      } else {
        Bill.update({ id: this.bill.id }, data).then(function (response) {
          _this.$dispatch('change-info');
          _this.$router.go({ name: 'bill-pay.list' });
        });
      }
    },
    getBill: function getBill(id) {
      var _this2 = this;

      Bill.get({ id: id }).then(function (response) {
        _this2.bill = new BillPay(response.data);
      });
    },
    getDueDate: function getDueDate(date_due) {
      var dateDueObject = date_due;
      if (!(date_due instanceof Date)) {
        dateDueObject = new Date(date_due.split('/').reverse().join('-') + 'T03:00:00');
      }
      return dateDueObject.toISOString().split('T')[0];
    }
  }
});
//# sourceMappingURL=bill-pay-create.component.js.map